var express = require('express');
var router = express.Router();
var oracledb = require('oracledb');
/* Alumnos */

router.get('/', function (req, res, next) {
	res.send('Welcome to UAQ API');
});

router.get('/apispfi/alumno/find/:expediente', function (req, res, next) {
	var sql = [
		"SELECT * FROM SERVICIOS.VALUMNOS_SITP WHERE EXPED = :expediente",
	].join(' ');
	UAQ.query(sql, [parseInt(req.params.expediente)], {}, function (err, result) {
		if (!err) {
			if (result != null) {
				var json = UAQ.getJSONObject(result);
				res.json({
					code: 200,
					message: 'OK',
					result: json
				});
			} else {
				res.json({
					code: 404,
					message: 'No se encontró ningún registro con los parámetros de búsqueda especificados.',
					result: []
				});
			}
		} else {
			res.json({
				code: 500,
				message: err,
				result: null
			});
		}
	});
});

router.get('/apispfi/alumno/find/:expediente/:nip', function (req, res, next) {
	var expediente = req.params.expediente;
	// obtener clave
	var nip = String(req.params.nip);
	var login = false;
	var statement = 'SELECT SERVICIOS.VALIDANIPESTUDIANTE(' + expediente + ', \'' + nip + '\') FROM DUAL';
	UAQ.query(statement, {}, {}, function (err, result) {
		if (!err) {
			if (result != null) {
				login = result.rows[0][0] == 'OK' ? true : false;
				res.json({
					code: 200,
					message: 'OK',
					result: login
				});
			} else {
				res.json({
					code: 404,
					message: 'No se encontró ningún registro con los parámetros de búsqueda especificados.',
					result: []
				});
			}
		} else {
			res.json({
				code: 500,
				message: err,
				result: null
			});
		}
	});
});


router.get('/apispfi/trabajador/find/:expediente/:nip', function (req, res, next) {
	var expediente = req.params.expediente;
	// obtener clave
	var nip = String(req.params.nip);
	var login = false;
	// var statement = 'SELECT SERVICIOS.VALIDANIPESTUDIANTE(' + expediente + ', \'' + nip + '\') FROM DUAL';
	var statement = 'BEGIN PR_INFO_SITP('+expediente+',\''+nip+'\', :pNipCorrecto, :pNombre); END;'
	var bindvars = { 
		pNipCorrecto: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 40 },
		pNombre: { dir: oracledb.BIND_OUT, type: oracledb.STRING, maxSize: 40 }
	}
	UAQ.procedure(statement, bindvars, {}, function (err, result) {
		if (!err) {
			if (result != null) {
				res.json({
					code: 200,
					message: 'OK',
					result: result.outBinds
				});
			} else {
				res.json({
					code: 404,
					message: 'No se encontró ningún registro con los parámetros de búsqueda especificados.',
					result: []
				});
			}
		} else {
			res.json({
				code: 500,
				message: err,
				result: null
			});
		}
	});
});


module.exports = router;