var oracledb = require('oracledb');
var dbConfig = require('../config/uaq.js');

exports.openConnection = function(callback) {
	oracledb.maxRows = 1000;
	oracledb.getConnection(dbConfig, callback);
};

exports.releaseConnection = function(connection) {
	connection.close(function(err) {
		if (err) {
			console.error(err.message);
		}
	});
}

exports.query = function(statement, values, options, callback) {
	console.log('Executing (UAQ):', statement);
	UAQ.openConnection(function(err, connection) {
		if (!err) {
			connection.execute(
			statement,
			values,
			options,
			function(err, result) {
				if (!err) {
					if (result.rows.length > 0) {
						UAQ.releaseConnection(connection);
						callback(null, result);
					} else {
						UAQ.releaseConnection(connection);
						callback(null, null);
					}
				} else {
					UAQ.releaseConnection(connection);
					callback(err.message, null);
				}
			});	
		} else {
			callback(err.message, null);
		}
	});
}

exports.procedure = function(statement, values, options, callback) {
	console.log('Executing (UAQ):', statement);
	UAQ.openConnection(function(err, connection) {
		if (!err) {
			connection.execute(
			statement,
			values,
			options,
			function(err, result) {
				if (!err) {
						UAQ.releaseConnection(connection);
						callback(null, result);
				} else {
					UAQ.releaseConnection(connection);
					callback(err.message, null);
				}
			});	
		} else {
			callback(err.message, null);
		}
	});
}

exports.getJSONObject = function(resultset) {
	var cols = resultset.metaData;
	var rows = resultset.rows;
	var json = {};
	for (i = 0; i < cols.length; i++) {
		eval('json.' + cols[i].name + ' = rows[0][i]');
	}
	return json;
}

exports.getJSONArray = function(resultset, columnsQuantity) {
	var cols = resultset.metaData;
	var rows = resultset.rows;
	var array = [];
	for (i = 0; i < rows.length; i++) {
		var json = {};
		for (j = 0; j < columnsQuantity; j++) {
			eval('json.' + cols[j].name + ' = rows[i][j]');
		}
		array.push(json);
	}
	return array;
}
